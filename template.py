#!/usr/bin/env python3
"""
Docstring
"""

__author__ = 'Your name'
__version__ = '0.1'
__license__ = 'MIT'

from sys import exit
from platform import python_version, machine, system
from os import utime, makedirs
from os.path import exists, dirname

from logging import getLogger
from logging.config import fileConfig
import logging.handlers

from lib.config import config
from lib.parser import Parser


def main():
    """ Main """


if __name__ == '__main__':
    try:
        parser = Parser()
        cf = config(parser.config_file)

        if exists(parser.default_logfile) is False:
            basedir = dirname(parser.default_logfile)
            if not  exists(basedir):
                makedirs(basedir)
            with open(parser.default_logfile, 'a'):
                utime(parser.default_logfile, None)

        __loggername__ = parser.app_name.title()
        fileConfig(fname=parser.default_logconf)
        log = getLogger(__loggername__)
        log.debug('%s-%s running on Python version: %s-%s on %s',
                  __loggername__,
                  __version__,
                  python_version(),
                  machine(),
                  system())
        log.debug('Configuration settings:\n\n \
Approot: %s |\n \
Logconfig file: %s |\n \
Config file: %s |\n',
                  parser.app_root,
                  parser.default_logconf,
                  parser.config_file)

        main()
    except Exception:
        print('Something went wrong')
        exit(1)
